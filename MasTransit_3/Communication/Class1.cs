﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public class FirstBid
    {
        public string BidValue { get; set; }
    }

    public class Bid: CorrelatedBy<Guid>
    {
        public string BidValue { get; set; }
        public Guid CorrelationId { get; set; }
    }

    public class QuestionForNewBid
    {
        public Guid id { get; set; }
    }

    public class ToBidderMessage
    {
        public string Message { get; set; }
    }
}
