﻿using Communication;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client_2
{
    class Program
    {
        public static Guid correlationID;
        public static Task NewSagaHandle(ConsumeContext<QuestionForNewBid> ctx)
        {
            correlationID = ctx.Message.id;
            return Task.CompletedTask;
            //return Console.Out.WriteLineAsync($"CorrelationID_Sagi: {ctx.Message.id}");
        }

        public static Task NewActionMessageHandle(ConsumeContext<ToBidderMessage> ctx)
        {
            return Console.Out.WriteLineAsync($"CorrelationID_Sagi: {ctx.Message.Message}");
        }

        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc => {
                var host = sbc.Host(new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => { h.Username("xtgdkfmw"); h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf"); });
                sbc.ReceiveEndpoint(host, "queue33", ep => {
                    ep.Handler<QuestionForNewBid>(NewSagaHandle);
                    ep.Handler<ToBidderMessage>(NewActionMessageHandle);
                });
            });
            bus.Start();

            while (true)
            {
                Console.WriteLine("Type your offer: ");
                var newOffer = Console.ReadLine();
                var newBid = new Bid() { BidValue = newOffer, CorrelationId = correlationID };
                bus.Publish(newBid);
            }
        }
    }
}
