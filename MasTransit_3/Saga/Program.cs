﻿using Automatonymous;
using Communication;
using MassTransit;
using MassTransit.Saga;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saga
{
    public class Timeout : CorrelatedBy<Guid>
    {
        public Guid CorrelationId { get; set; }
    }

    public class AuctionData : SagaStateMachineInstance
    {
        public Guid? TimeoutId { get; set; }
        public Guid CorrelationId { get; set; }
        public string CurrentState { get; set; }
        public string Price { get; set; }
    }

    public class AuctionSaga : MassTransitStateMachine<AuctionData>
    {
        // States
        public State Completed { get; private set; }
        public State Stared { get; private set; }
        public State InProgress { get; private set; }

        // Events
        public Event<FirstBid> FirstBid { get; private set; }
        public Event<Bid> Bid { get; private set; }
        public Event<Timeout> TimeoutEvt { get; private set; }
        public Schedule<AuctionData, Timeout> TO { get; private set; }

        public AuctionSaga()
        {
            Schedule(() => TO,
                x => x.TimeoutId,
                x => { x.Delay = TimeSpan.FromSeconds(30); }); 

            InstanceState(x => x.CurrentState);

            Event(
                () => FirstBid,
                x => x.CorrelateBy(v => v.Price, ctx => ctx.Message.BidValue)
                      .SelectId(context => Guid.NewGuid()));

            Initially(
                When(FirstBid)
                    .Then(context => { })
                    .ThenAsync(ctx =>
                    {
                        ctx.Instance.Price = ctx.Data.BidValue;
                        return Console.Out.WriteLineAsync(
                            $"value={ctx.Data.BidValue}\n" +
                            $"id={ctx.Instance.CorrelationId}\n");
                    })
                    .Publish(ctx =>
                    {
                        return new QuestionForNewBid() { id = ctx.Instance.CorrelationId };
                    })
                    .TransitionTo(Stared)
            );

            During(Stared,
                When(Bid)
                    .Schedule(TO, ctx => new Timeout() { CorrelationId = ctx.Instance.CorrelationId })
                    .Then(ctx =>
                    {
                        if (Convert.ToInt32(ctx.Data.BidValue) > Convert.ToInt32(ctx.Instance.Price))
                        {
                            ctx.Instance.Price = ctx.Data.BidValue;
                        }
                        Console.WriteLine("New offer: " + ctx.Data.BidValue);
                    })
                    .Publish(ctx =>
                    {
                        return new ToBidderMessage() { Message = "New offer: " + ctx.Data.BidValue };
                    })
                    .TransitionTo(InProgress));

            During(InProgress,
                When(Bid)
                    //.Then(context => { })
                    .Then(ctx =>
                    {
                        if (Convert.ToInt32(ctx.Data.BidValue) > Convert.ToInt32(ctx.Instance.Price))
                        {
                            ctx.Instance.Price = ctx.Data.BidValue;
                        }
                        Console.WriteLine("New offer: " + ctx.Data.BidValue);
                    })
                    .Publish(ctx =>
                    {
                        return new ToBidderMessage() { Message = "New offer: " + ctx.Data.BidValue };
                    }));


            During(InProgress,
                When(TimeoutEvt)
                    .Then(ctx => { Console.WriteLine("THE END - The winner is: " + ctx.Instance.Price); })
                    .Publish(ctx =>
                    {
                        return new ToBidderMessage() { Message = "THE END - The winner is: " + ctx.Instance.Price };
                    })
                    .Finalize());

            SetCompletedWhenFinalized();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var repo = new InMemorySagaRepository<AuctionData>();
            var machine = new AuctionSaga();

            var bus = Bus.Factory.CreateUsingRabbitMq(sbc => {
                var host = sbc.Host(new Uri("rabbitmq://llama.rmq.cloudamqp.com/xtgdkfmw"),
                h => { h.Username("xtgdkfmw"); h.Password("htUtcmGXLGbe1ZkCH3mj6U8JHHb8XWLf"); });
                sbc.ReceiveEndpoint(host, "queue1", ep =>
                {
                    ep.StateMachineSaga(machine, repo);
                });
                sbc.UseInMemoryScheduler();
            });
            bus.Start();
            Console.ReadKey();
        }
    }
}
